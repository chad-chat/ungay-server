/*

*/

#include "main.h"

#define THIS_THREAD MAIN_THREAD

#include <sqlite3.h>
#include <string>

static sqlite3 *db;

static std::vector<std::vector<std::string>> qResult;
static std::vector<std::string> qResultCols;

int callback(void* queryid, int ncols, char** row, char** cols) {
    switch (*(int*)queryid) {
        case 1:
        case 2:
        case 3:
        case 5: return 0; break;
        case 4:
        case 6: {
            std::vector<std::string> vrow;
            qResultCols.clear();
            for (int i = 0; i < ncols; i++) {
                vrow.push_back(row[i]);
                qResultCols.push_back(cols[i]);
            }
            qResult.push_back(vrow);
        } return 0; break;
    }

    return 0;
}

bool initDatabase() {
    std::string dbpath = std::string(conf["dbroot"]) + "/db.sqlite3";
    if (sqlite3_open(dbpath.c_str(), &db) != SQLITE_OK) {
        log(LOG_ERROR, THIS_THREAD, std::string("Could not open SQLite database [") + dbpath + "]: "
            + sqlite3_errmsg(db));
        return false;
    }

    // check tables
    int qid = 1;
    char *errmsg = nullptr;

    std::string qCreateMsgs = "CREATE TABLE IF NOT EXISTS messages ("
        "id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,"
        "username VARCHAR NOT NULL,"
        "message VARCHAR NOT NULL,"
        "timestamp BIGINT NOT NULL);";

    std::string qCreateUsers = "CREATE TABLE IF NOT EXISTS users ("
        "username VARCHAR NOT NULL PRIMARY KEY,"
        "password VARCHAR NOT NULL);";

    if (sqlite3_exec(db, qCreateMsgs.c_str(), callback, &qid, &errmsg)) {
        log(LOG_ERROR, MAIN_THREAD, std::string("SQL Error [qid=") + std::to_string(qid) + std::string("]: ") + errmsg);
        sqlite3_free(errmsg);
    } 
    qid++;
    if (sqlite3_exec(db, qCreateUsers.c_str(), callback, &qid, &errmsg)) {
        log(LOG_ERROR, MAIN_THREAD, std::string("SQL Error [qid=") + std::to_string(qid) + std::string("]: ") + errmsg);
    }

    return true;
}

// check sizes beforehand
void createUser(std::string username, std::string passwd) {
    std::string qCreateUser = std::string("INSERT INTO users VALUES ('") + username
        + std::string("', '") + passwd + "')";
    
    int qid = 3;
    char *errmsg = nullptr;
    if (sqlite3_exec(db, qCreateUser.c_str(), callback, &qid, &errmsg)) {
        log(LOG_ERROR, MAIN_THREAD, std::string("SQL Error [qid=") + std::to_string(qid) + std::string("]: ") + errmsg);
        sqlite3_free(errmsg);
    } else log(LOG_INFO, MAIN_THREAD, std::string("Created user ") + username);
}

bool authenticateUser(std::string username, std::string passwd) {
    std::string qGetUser = std::string("SELECT * FROM users WHERE username = '") + username + "';";

    int qid = 4;
    char *errmsg = nullptr;
    qResult.clear();
    if (sqlite3_exec(db, qGetUser.c_str(), callback, &qid, &errmsg)) {
        log(LOG_ERROR, MAIN_THREAD, std::string("SQL Error [qid=") + std::to_string(qid) + std::string("]: ") + errmsg);
        sqlite3_free(errmsg);
    }

    if (qResult.size() > 0)
        if (qResult[0].size() > 1)
            return username == qResult[0][0] && passwd == qResult[0][1];
    else {
        log(LOG_ERROR, MAIN_THREAD, "Authentication failed, query resulted in weird result, likely username doesn't exist");
    }

    return false;
}

// check sizes beforehand
void pushMessage(std::string username, std::string message) {
    std::string qPushMessage = std::string("INSERT INTO messages (username, message, timestamp) VALUES ('")
     + username + std::string("', '") + message + "');";

    int qid = 5;
    char *errmsg = nullptr;
    if (sqlite3_exec(db, qPushMessage.c_str(), callback, &qid, &errmsg)) {
        log(LOG_ERROR, MAIN_THREAD, std::string("SQL Error [qid=") + std::to_string(qid) + std::string("]: ") + errmsg);
        sqlite3_free(errmsg);
    } else log(LOG_INFO, MAIN_THREAD, std::string("Pushed message from ") + username);
}

std::vector<Msg> getMessages(int bsize, int bn) {
    std::string qGetUser = std::string("SELECT * FROM messages ORDER BY id DESC LIMIT ")
        + std::to_string(bsize) + std::string(" OFFSET ") + std::to_string(bsize * bn);

    int qid = 6;
    char *errmsg = nullptr;
    qResult.clear();
    if (sqlite3_exec(db, qGetUser.c_str(), callback, &qid, &errmsg)) {
        log(LOG_ERROR, MAIN_THREAD, std::string("SQL Error [qid=") + std::to_string(qid) + std::string("]: ") + errmsg);
        sqlite3_free(errmsg);
    }

    std::vector<Msg> msgs;
    if (qResult.size() > 0) {
        for (std::vector<std::string>& row : qResult) {
            Msg msg;
            msg.msgid = std::stoi(row[0]);
            msg.username = row[1];
            msg.text = row[2];
            msg.timestamp = std::stoi(row[3]);
            msgs.push_back(msg);
        }
    }
    else log(LOG_ERROR, MAIN_THREAD, "Database is empty or no more messages, I guess?");

    return msgs;
}