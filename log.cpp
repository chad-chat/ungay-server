/*

*/

// Includes
#include "ansicolor.h"
#include "main.h"

#include <string>
#include <queue>
#include <thread>
#include <atomic>
#include <mutex>
#include <chrono>
#include <ctime>
#include <iomanip>
#include <sstream>
#include <iostream>

//#include <Windows.h>

color::Modifier yel(color::FG_DARK_YELLOW);
color::Modifier red(color::FG_RED);
color::Modifier gre(color::FG_GREEN);
color::Modifier def(color::DEFAULT);

#define THIS_THREAD	LOG_THREAD

// Structs
struct logItem {
	int type;
	int from;
	std::string time;
	std::string text;
};

// File globals
std::queue<logItem> logQueue;
std::mutex logMutex;

std::atomic_bool log_stop = false;

// Functions

std::string time()
{
	using namespace std::chrono;
	auto now = system_clock::now();
	auto ms = duration_cast<milliseconds>(now.time_since_epoch()) % 1000;
	auto timer = system_clock::to_time_t(now);
	//std::tm bt = *std::localtime(&timer);
	std::tm bt;
	localtime_r(&timer, &bt);
	std::ostringstream oss;
	oss << std::put_time(&bt, "%H:%M:%S"); // HH:MM:SS
	oss << '.' << std::setfill('0') << std::setw(3) << ms.count();
	return oss.str();
}

// Global

void log(int type, int from, std::string text) {
	logMutex.lock();
	logQueue.push(logItem{ type, from, time(), text });
	logMutex.unlock();
}


// Thread

std::string fromString(int from) {
	switch (from) {
	case MAIN_THREAD:		return "Main thread";
	case LOG_THREAD:		return "Log thread";
	case SERVER_THREAD:		return "Server thread";
	case HANDLER_THREAD:	return "Handler thread";
	default:				return "Unknown thread";
	}
}

std::string typeString(int from) {
	switch (from) {
	case LOG_INFO:			return "INFO";
	case LOG_WARNING:		return "WARNING";
	case LOG_ERROR:			return "ERROR";
	case LOG_UNRECOVERABLE:	return "UNRECOVERABLE";
	case LOG_SUCCESS:		return "SUCCESS";
	default:			return "UNKNOWN";
	}
}

void typeColor(int type) {
	switch (type) {
	case LOG_INFO:			return;
	case LOG_WARNING:		std::cout << yel; return;
	case LOG_ERROR:			std::cout << red; return;
	case LOG_UNRECOVERABLE:	std::cout << red; return;
	case LOG_SUCCESS:		std::cout << gre; return;
	default:			return;
	}
}

void logLoop() {
	/*
	HANDLE hInput = GetStdHandle(STD_INPUT_HANDLE), hOutput = GetStdHandle(STD_OUTPUT_HANDLE);
	DWORD dwMode;
	GetConsoleMode(hOutput, &dwMode);
	dwMode |= ENABLE_PROCESSED_OUTPUT | ENABLE_VIRTUAL_TERMINAL_PROCESSING;
	if (!SetConsoleMode(hOutput, dwMode)) {
		std::cout << "SetConsoleMode failed. Color disabled." << std::endl;
	}*/

	log(LOG_INFO, THIS_THREAD, "Log started");

	while (true) {
		logMutex.lock();

		if (logQueue.size() > 0) {
			logItem item = logQueue.front();
			logQueue.pop();
			logMutex.unlock();

			std::cout << "[" << item.time << "] [" << fromString(item.from) << "/";
			typeColor(item.type);
			std::cout << typeString(item.type) << def << "]: " << item.text << std::endl;
		}
		else {
			if (log_stop) { log_stop = false; return; }
			logMutex.unlock();
		}

		std::this_thread::sleep_for(1ms);
	}
}

void waitStopLog() {
	log_stop = true;
	while (log_stop) { }
}