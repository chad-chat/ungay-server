/*

TODO

main.h:			connector

main.cpp:		Init stuff, start threads
server.cpp:		Thread: Listen on socket, accept clients, interpret client data, push client commands in the command queue, push log to log queue
handler.cpp:	Thread: Pop commands from command queue and execute them, push log to log queue
log.cpp:		Thread: Pop log from log queue and print it
console.cpp:	Thread: Create second console window, listen for admin commands

*/

/*

*/

// Includes
#include "main.h"

#include <string>
#include <thread>
#include <fstream>
#include <filesystem>
#include <chrono>
#include <iostream>

#undef max

#define THIS_THREAD	MAIN_THREAD

constexpr auto confPath = "server.conf";
constexpr auto channelsFileName = "channels.json";

json conf;
json channels;
json users;

void stopServer(int c) {
	//std::this_thread::sleep_for(1000ms);
	waitStopLog();
	exit(c);
}

bool checkConf() {
	//log(LOG_INFO, THIS_THREAD, "checking conf");
	if (!std::filesystem::exists(confPath)) {
		log(LOG_WARNING, THIS_THREAD, "No server.conf, creating default one");
		std::ofstream confFile(confPath);
		json defaultConf = {
			{ "port", "9090"},
			{ "dbroot", "" }
		};
		confFile << defaultConf;
		confFile.close();
	}

	std::ifstream confFile(confPath);
	try {
		confFile >> conf;
	}
	catch (json::exception& e) {
		log(LOG_ERROR, THIS_THREAD, "Stopping server on malformed JSON conf: " + std::string(e.what()));
		stopServer(1);
	}

	if (!conf.contains("port")) { log(LOG_ERROR, THIS_THREAD, "Port not defined in server.conf"); return false; }
	if (conf["port"].type() != json::value_t::number_unsigned) { log(LOG_ERROR, THIS_THREAD, "Port is NaN"); return false; }
	if (!conf.contains("dbroot")) { log(LOG_ERROR, THIS_THREAD, "dbroot not defined in server.conf"); return false; }
	if (conf["dbroot"].type() != json::value_t::string) { log(LOG_ERROR, THIS_THREAD, "dbroot is NaS"); return false; }

	return true;
}

bool checkDB() {
	std::string path = conf["dbroot"];

	return true;
}

int main(int argc, char **argv) {
	log(LOG_INFO, THIS_THREAD, "Starting server...");

	// Start threads
	std::thread logThread(logLoop);				// Log thread
	logThread.detach();

	if (!checkConf()) {
		log(LOG_ERROR, THIS_THREAD, "Stopping server on misconfiguration");
		stopServer(1);
	}
	else log(LOG_SUCCESS, THIS_THREAD, "Configuration loaded");



	if (!initDatabase()) {
		log(LOG_ERROR, THIS_THREAD, "Stopping server on database failure");
		stopServer(1);
	}
	else log(LOG_SUCCESS, THIS_THREAD, "Database ready");

	std::thread handlerThread(handlerLoop);		// Handler thread
	handlerThread.detach();

	if (!startServer()) {						// Server thread (on his own)
		log(LOG_WARNING, THIS_THREAD, "Stopping server on fail");
		std::this_thread::sleep_for(1000ms);
		waitStopLog();
		stopServer(1);
	}

	std::this_thread::sleep_for(3s);

	createUser("arf20", "asdfa");

	if (authenticateUser("arf20", "asdf")) log(LOG_INFO, THIS_THREAD, "sucess"); else log(LOG_INFO, THIS_THREAD, "fail");

	// sleep main thread
	std::this_thread::sleep_until(std::chrono::time_point<std::chrono::system_clock>::max());
	stopServer(0);
}