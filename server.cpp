/*

*/

// Includes
#include "arfTCP.h"
#include "json.hpp"
#include "main.h"
// STD includes
#include <fstream>


#define THIS_THREAD	SERVER_THREAD

// 
using json = nlohmann::json;

// Handlers

bool acceptHandler(arf::TCPBase* client, arf::SockAddr addr) {
	log(LOG_INFO, THIS_THREAD, "Client connected " + addr.ToString());
	return true;
}

void disconnectHandler(arf::SockAddr addr, arf::Exception e) {
	if (e.e)
		log(LOG_ERROR, THIS_THREAD, e.ToString());
	log(LOG_INFO, THIS_THREAD, "Client disconnected " + addr.ToString());
}

bool recvHandler(arf::TCPBase* sock, const char *buffer, size_t size) {
	std::string buffStr(buffer);
	if (buffStr != "") {
		try {
			command(UserCmd{ sock, json::parse(buffStr) });
		}
		catch (json::exception& e) {
			log(LOG_WARNING, THIS_THREAD, "Bad JSON command " + std::string(e.what()));
		}
	}
	else {
		//log(WARNING, THIS_THREAD, "Empty buffer");
	}
	return true;
}

bool startServer() {
	arf::TCPInit();

	arf::TCPServerMulti *server = nullptr;

	try {
		server = new arf::TCPServerMulti(arf::SockAddr(ARF_ADDR_ANY, conf["port"]), acceptHandler, recvHandler, disconnectHandler);
		log(LOG_SUCCESS, THIS_THREAD, "Socket started");
	}
	catch (arf::Exception e) {
		log(LOG_ERROR, THIS_THREAD, e.ToString());
		return false;
	}

	return true;
}