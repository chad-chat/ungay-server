/*

*/

#include "json.hpp"
#include "main.h"

#include <queue>
#include <thread>
#include <mutex>
#include <atomic>

using json = nlohmann::json;

// File globals
std::queue<UserCmd> commandQueue;
std::mutex commandMutex;

std::atomic_bool handler_stop(false);

#define THIS_THREAD	HANDLER_THREAD

void command(UserCmd usercmd) {
	commandMutex.lock();
	commandQueue.push(usercmd);
	commandMutex.unlock();
}



void handlerLoop() {
	log(LOG_INFO, THIS_THREAD, "Handler started");

	while (true) {
		if (handler_stop) { handler_stop = false; return; }

		if (commandQueue.size() > 0) {
			commandMutex.lock();
			UserCmd usercmd = commandQueue.front();
			json cmd = usercmd.cmd;
			commandQueue.pop();
			commandMutex.unlock();

			try  {
				log(LOG_INFO, THIS_THREAD, "Received command: " + std::string(cmd["cmd"]));

				if (cmd["cmd"] == "GET_MSGS") {			// answers with MSGS
					
				} else if (cmd["cmd"] == "SEND_MSG") {	// answers with SEND_MSG_ACK

				}
			}
			catch (json::exception& e) {
				log(LOG_WARNING, THIS_THREAD, "Bad command " + std::string(e.what()));
			}
			
			commandMutex.unlock();
		}

		std::this_thread::sleep_for(1ms);
	}
}

void stopHandler() {
	handler_stop = true;
	while (handler_stop) { }
}