# create tables

CREATE TABLE IF NOT EXISTS messages (
  id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  username VARCHAR(14) NOT NULL,
  message VARCHAR(max) NOT NULL,
  timestamp BIGINT NOT NULL
);

CREATE TABLE IF NOT EXISTS users (
  username VARCHAR(14) NOT NULL PRIMARY KEY,
  password VARCHAR(64) NOT NULL
);

# create user

INSERT INTO users
VALUES ("arf20", "hashedpasswd")

# authenticate user

SELECT * FROM users
WHERE username = "arf20"

# store message

INSERT INTO messages (username, message, timestamp)
VALUES ("arf20", "sql is gay")

# retrieve message block

SELECT * FROM messages ORDER BY id DESC LIMIT 32 OFFSET 32
