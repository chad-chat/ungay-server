// arfTCP.h: arf20's TCP/IP library
// A simple and elegant TCP library for basic TCP servers and clients

//		Started		16-01-2021
//		Released	24-01-2022
//		Version 1.4d

#pragma once

#ifndef _ARFTCP_H
#define _ARFTCP_H

#if defined(__GNUC__) || defined(__clang__)
#define ARF_DEPRECATED __attribute__((deprecated))
#elif defined(_MSC_VER)
#define ARF_DEPRECATED __declspec(deprecated)
#else
#pragma message("WARNING: You need to implement DEPRECATED for this compiler")
#define ARF_DEPRECATED
#endif

// ==================================== INCLUDES ====================================

#ifdef _WIN32
// WinSock2
#include <WinSock2.h>
#include <WS2tcpip.h>
#pragma comment(lib, "Ws2_32.lib")
#elif defined __linux__
// POSIX sockets
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <errno.h>
#include <sys/ioctl.h>
#endif


// STD
#include <functional>
#include <string>
#include <thread>
#include <vector>
#include <chrono>
#include <atomic>
#include <stdexcept>

// ==================================== MACROS ====================================

// Internal buffer size
#ifndef ARF_BUFFSIZE
#define ARF_BUFFSIZE 65535
#endif

#define ARF_LOOKUP_PORT		"8080"

// Abstraction layer for WinSock types
#ifdef _WINSOCK2API_
#define ARF_SOCKET			SOCKET
#define ARF_SOCKADDR_IN		sockaddr_in
#define ARF_INVALID_SOCKET	INVALID_SOCKET
#define ARF_SOCKET_ERROR	SOCKET_ERROR
#define ARF_EWOULDBLOCK		WSAEWOULDBLOCK
#define ARF_SHUT_BOTH		SD_BOTH
#define ARF_ADDR_ANY		ADDR_ANY
#define ARF_CLOSE(fd)		closesocket(fd)
#elif defined __linux__
#define ARF_SOCKET			int
#define ARF_SOCKADDR_IN		sockaddr_in
#define ARF_INVALID_SOCKET	(ARF_SOCKET)(~0)
#define ARF_SOCKET_ERROR	(-1)
#define ARF_EWOULDBLOCK		EWOULDBLOCK
#define ARF_SHUT_BOTH		SHUT_RDWR
#define ARF_ADDR_ANY		0x0000
#define ARF_CLOSE(fd)		close(fd)
//#define FIONBIO				
#endif

#ifdef _WIN32
#define ARF_SINADDR			sin_addr.S_un.S_addr
#else
#define ARF_SINADDR			sin_addr.s_addr
#endif

// Callbacks
#define ARF_ERROR_HANDLER			std::function<void(int)> errorHandler
#define ARF_RECV_HANDLER			std::function<bool(TCPBase*, char*, size_t)> recvHandler
#define ARF_ACCEPT_HANDLER			std::function<bool(TCPBase*, SockAddr)> acceptHandler
#define ARF_DISCONNECT_HANDLER		std::function<void(SockAddr, arf::Exception)> disconnectHandler
//#define ARF_THREAD_ERROR_HANDLER	std::function<void(arf::Exception)> threadErrorHandler

using namespace std::literals::chrono_literals;

// ==================================== DECLARATIONS ====================================

// arf namespace
namespace arf {
	// arf Exception
	class Exception {
	private:
		std::vector<std::string> msgs{
			// General
			"No error.",
			"Successful TCPInit() not yet performed.",
			"Network is down.",
			"Network is unreachable.",
			"Network dropped connection on reset.",
			"Alredy done.",
			"Unknown internal arfTCP error.",
			"Unknown internal unrecoverable arfTCP error.",
			// socket create
			"No more socket descriptors are available.",
			// Arguments
			"Invalid address specified.",
			"Hostname not found, or no A entry found.",
			// Connection general
			"Software caused connection abort.",
			"Connection reset by peer.",
			// Connect
			"Connection refused by peer.",
			"Connection timed out."
		};

		// socket(), 
	public:
		enum types {
			// General
			NOERROROK = 0,
			NOTINITIALISED,
			NETDOWN,
			NETUNREACH,
			NETRESET,
			ALREDY,
			UNKOWN,				// everything else
			UNRECOVERABLE,		// NOBUFS, 
			// socket create
			NOMORESOCK,
			// Arguments
			INVALIDADDR,
			NOTRESOLVED,
			// Connection general
			CONNABORTED,
			CONNRESET,
			// Connect
			CONNREFUSED,
			TIMEDOUT,
			// Listen
			ADDRINUSE,
		} e;

		int APIErrorCode;

		Exception() = default;
		Exception(types e, int err);
		std::string ToString();
	};

	// Handle error
	void HandleError(int err, ARF_ERROR_HANDLER);

	// Parse error
	arf::Exception ParseAPIError(int err);

	// Initialize WinSock
	void TCPInit();
	void TCPCleanup();

	// Get last error
	int APIGetLastError();

	// Resolve hostname or text IP address
	unsigned long ResolveHost(const char *name, ARF_ERROR_HANDLER = nullptr);
	unsigned long ResolveHost(std::string& name, ARF_ERROR_HANDLER = nullptr);

	// IPv4 Address and port class
	class SockAddr {
	private:
		ARF_SOCKADDR_IN addr = { };
	public:
		// Constructor from API structure
		SockAddr(ARF_SOCKADDR_IN laddr);
		// Constructor from long IPv4 address, and short port
		SockAddr(unsigned long iaddr, unsigned short port);
		// Address and port setters
		void ResetAddr(unsigned long iaddr);
		void ResetPort(unsigned short port);
		// Address and port getters
		unsigned long GetAddr();
		unsigned short GetPort();
		// Get API structure
		ARF_SOCKADDR_IN GetSockAddr();
		// Get address and port in the dotted format string
		std::string ToString();
	};

	// API socket and API address structure
	struct AddrSock {
		ARF_SOCKET sock;
		ARF_SOCKADDR_IN addr;
	};

	// Base class, common to servers and clients
	class TCPBase {
	private:
		std::thread recvThread;
		std::atomic_bool exitThreads{ false };
		void RecvLoop(ARF_RECV_HANDLER, ARF_DISCONNECT_HANDLER, ARF_ERROR_HANDLER = nullptr);
	protected:
		ARF_SOCKET sock = ARF_INVALID_SOCKET;
		bool isConnected = false;
		void ThreadHandleError(int err, ARF_DISCONNECT_HANDLER);
	public:
		// Contructor
		TCPBase() noexcept(false);
		TCPBase(const TCPBase&);
		TCPBase(ARF_SOCKET s, SockAddr addr);
		// Peer address
		SockAddr peerSockAddr = SockAddr(0, 0);
		// Send buffer to peer
		void Send(const char *buff, size_t size, ARF_ERROR_HANDLER = nullptr);
		// Receive buffer from peer
		void Recv(char *buff, size_t size, ARF_ERROR_HANDLER = nullptr);
		// Receive loop
		void StartRecvLoop(ARF_RECV_HANDLER, ARF_DISCONNECT_HANDLER, ARF_ERROR_HANDLER = nullptr);
		// Stop loop
		void StopRecvLoop();
		// Check connection
		bool IsConnected();
		// Disconnect and close
		void Disconnect(ARF_ERROR_HANDLER);
		// Close
		void Close(ARF_ERROR_HANDLER);
		// Destructor
		//~TCPBase();
	};

	// TCP single client server
	class TCPServer : public TCPBase {
	private:
		ARF_SOCKET listenSock = ARF_INVALID_SOCKET;
	public:
		// Constructor, block until client connects
		TCPServer(SockAddr listenSockAddr, ARF_ERROR_HANDLER = nullptr) noexcept(false);
		TCPServer(unsigned short listenPort, ARF_ERROR_HANDLER = nullptr) noexcept(false);
	};

	// TCP Client class
	class TCPClient : public TCPBase {
	public:
		// Constructor, listen and block until connect or error
		TCPClient(SockAddr peerSockAddr, ARF_ERROR_HANDLER = nullptr) noexcept(false);
		TCPClient(SockAddr peerSockAddr, ARF_RECV_HANDLER, ARF_DISCONNECT_HANDLER, ARF_ERROR_HANDLER = nullptr) noexcept(false);
	};

	class TCPServerMulti {
	private:
		ARF_SOCKET listenSock = ARF_INVALID_SOCKET;
		std::atomic_bool exitThreads{ false };
		std::thread acceptThread;
		std::thread recvThread;
		std::vector<AddrSock> clients;
		void HandleError(int err, ARF_ERROR_HANDLER = nullptr);
		void ThreadHandleError(SockAddr addr, int err, ARF_DISCONNECT_HANDLER);
		void AcceptLoop(ARF_ACCEPT_HANDLER, ARF_DISCONNECT_HANDLER);
		void RecvLoop(ARF_RECV_HANDLER, ARF_DISCONNECT_HANDLER);
	public:
		// Constructor, listen and start accept loop
		TCPServerMulti() = default;
		TCPServerMulti(TCPServerMulti&) = default;
		TCPServerMulti(SockAddr listenSockAddr, ARF_ACCEPT_HANDLER, ARF_RECV_HANDLER, ARF_DISCONNECT_HANDLER, ARF_ERROR_HANDLER = nullptr) noexcept(false);
		void StopThreads();
		void SendBroadcast(char *buff, size_t size, ARF_ERROR_HANDLER = nullptr);
		size_t GetClientCount();
		TCPBase* GetClient(int idx);
		SockAddr GetClientAddr(int idx);
		void Close(ARF_ERROR_HANDLER = nullptr);
		//~TCPServerMulti();
	};
}



// ==================================== IMPLEMENTATION ====================================

inline arf::Exception::Exception(types le, int err) {
	e = le;
	APIErrorCode = err;
}

inline std::string arf::Exception::ToString() {
	return msgs[e];
}

inline arf::Exception arf::ParseAPIError(int err) {
	//throw std::runtime_error("Unhandled error: " + std::to_string(err));
	Exception e;

	switch (err) {
#ifdef _WIN32
	case WSANOTINITIALISED:		e = Exception(Exception::NOTINITIALISED, err); break;
	case WSAENETDOWN:			e = Exception(Exception::NETDOWN, err); break;
	case WSAEMFILE:				e = Exception(Exception::NOMORESOCK, err); break;
	case WSAENOBUFS:			e = Exception(Exception::UNRECOVERABLE, err); break;
	case WSA_NOT_ENOUGH_MEMORY:	e = Exception(Exception::UNRECOVERABLE, err); break;
	case WSAHOST_NOT_FOUND:		e = Exception(Exception::NOTRESOLVED, err); break;
	case WSANO_DATA:			e = Exception(Exception::NOTRESOLVED, err); break;
	case WSANO_RECOVERY:		e = Exception(Exception::UNRECOVERABLE, err); break;
	case WSAEADDRINUSE:			e = Exception(Exception::ADDRINUSE, err); break;
	case WSAEADDRNOTAVAIL:		e = Exception(Exception::INVALIDADDR, err); break;
	case WSAECONNREFUSED:		e = Exception(Exception::CONNREFUSED, err); break;
	case WSAEISCONN:			e = Exception(Exception::ALREDY, err); break;
	case WSAENETUNREACH:		e = Exception(Exception::NETUNREACH, err); break;
	case WSAEHOSTUNREACH:		e = Exception(Exception::NETUNREACH, err); break;
	case WSAETIMEDOUT:			e = Exception(Exception::TIMEDOUT, err); break;
	case WSAECONNRESET:			e = Exception(Exception::CONNRESET, err); break;
	case WSAENETRESET:			e = Exception(Exception::NETRESET, err); break;
	case WSAECONNABORTED:		e = Exception(Exception::CONNABORTED, err); break;
	default:					e = Exception(Exception::UNKOWN, err); break;
#else
	case EMFILE:				e = Exception(Exception::NOMORESOCK, err); break;
	case ENOBUFS:				e = Exception(Exception::UNRECOVERABLE, err); break;
	case ENOMEM:				e = Exception(Exception::UNRECOVERABLE, err); break;
	case EAI_NONAME:			e = Exception(Exception::NOTRESOLVED, err); break;
	case ENODATA:				e = Exception(Exception::NOTRESOLVED, err); break;
	case ENOTRECOVERABLE:		e = Exception(Exception::UNRECOVERABLE, err); break;
	case EADDRINUSE:			e = Exception(Exception::ADDRINUSE, err); break;
	case EADDRNOTAVAIL:			e = Exception(Exception::INVALIDADDR, err); break;
	case ECONNREFUSED:			e = Exception(Exception::CONNREFUSED, err); break;
	case EISCONN:				e = Exception(Exception::ALREDY, err); break;
	case ENETUNREACH:			e = Exception(Exception::NETUNREACH, err); break;
	case EHOSTUNREACH:			e = Exception(Exception::NETUNREACH, err); break;
	case ETIME:					e = Exception(Exception::TIMEDOUT, err); break;
	case ECONNRESET:			e = Exception(Exception::CONNRESET, err); break;
	case ENETRESET:				e = Exception(Exception::NETRESET, err); break;
	case ECONNABORTED:			e = Exception(Exception::CONNABORTED, err); break;
	default:					e = Exception(Exception::UNKOWN, err); break;
#endif
	}

	return e;
}

inline void arf::HandleError(int err, ARF_ERROR_HANDLER) {
	if (errorHandler != nullptr)
		errorHandler(err);
	else {
		//throw std::runtime_error("Unhandled error: " + std::to_string(err));
		throw(ParseAPIError(err));
		// NOTE: If you see a unhandled exception message here, you forgot to wrap a call in a try/catch block!
		// Remember, handle arf::Exception e.
	}
}

#ifdef _WIN32
// Initialize WinSock
inline void arf::TCPInit() {
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
}

inline void arf::TCPCleanup() {
	WSACleanup();
}
#else
//#warning TCPInit is WinSock2 only, no need to call it in POSIX
inline void arf::TCPInit() { }
inline void arf::TCPCleanup() { }
#endif

#ifdef _WIN32
inline int arf::APIGetLastError() {
	return WSAGetLastError();
}
#else
inline int arf::APIGetLastError() {
	return errno;
}
#endif

// Resolve hostname or text IP address
inline unsigned long arf::ResolveHost(const char *name, ARF_ERROR_HANDLER) {
	addrinfo hints = { 0, AF_INET, 0, 0, 0, nullptr, nullptr, nullptr }, *addr;

	if (getaddrinfo(name, ARF_LOOKUP_PORT, &hints, &addr) != 0)
		if (errorHandler) {
			errorHandler(APIGetLastError()); return 0;
		}

	sockaddr_in hostAddr = *(sockaddr_in*)addr->ai_addr;

	return hostAddr.ARF_SINADDR;
}

inline unsigned long arf::ResolveHost(std::string& name, ARF_ERROR_HANDLER) {
	return ResolveHost(name, errorHandler);
}

// Constructor from sockaddr_in
inline arf::SockAddr::SockAddr(ARF_SOCKADDR_IN laddr) {
	addr = laddr;
}

// Constructor from long IPv4 address, and short port
inline arf::SockAddr::SockAddr(unsigned long iaddr, unsigned short port) {
	addr.sin_family = AF_INET;
	addr.ARF_SINADDR = iaddr;
	addr.sin_port = htons(port);
}

inline void arf::SockAddr::ResetAddr(unsigned long iaddr) {
	addr.ARF_SINADDR = iaddr;
}

inline void arf::SockAddr::ResetPort(unsigned short port) {
	addr.sin_port = htons(port);
}

inline unsigned long arf::SockAddr::GetAddr() {
	return addr.ARF_SINADDR;
}

inline unsigned short arf::SockAddr::GetPort() {
	return ntohs(addr.sin_port);
}

inline sockaddr_in arf::SockAddr::GetSockAddr() {
	return addr;
}

inline std::string arf::SockAddr::ToString() {
	char buff[INET_ADDRSTRLEN];
	inet_ntop(AF_INET, &addr.sin_addr, buff, INET_ADDRSTRLEN);
	std::string strAddr = std::string(buff) + ":" + std::to_string(ntohs(addr.sin_port));
	return strAddr;
}

// ==== TCPBase ====
inline void arf::TCPBase::ThreadHandleError(int err, ARF_DISCONNECT_HANDLER) {
	if (disconnectHandler != nullptr)
		disconnectHandler(SockAddr(0x0000, 0), ParseAPIError(err));
	else throw std::runtime_error("Disconnect handler can't be null, it also handles internal thread errors.");
}

inline arf::TCPBase::TCPBase() {

}

inline arf::TCPBase::TCPBase(const TCPBase & t) {
	sock = t.sock;
	peerSockAddr = t.peerSockAddr;
}

inline arf::TCPBase::TCPBase(ARF_SOCKET s, SockAddr addr) {
	sock = s;
	peerSockAddr = addr;
}

inline void arf::TCPBase::Send(const char *buff, size_t size, ARF_ERROR_HANDLER) {
	if (send(sock, buff, size, 0) == ARF_SOCKET_ERROR) {
		HandleError(APIGetLastError(), errorHandler); isConnected = false; return;
	}
}

inline void arf::TCPBase::Recv(char *buff, size_t size, ARF_ERROR_HANDLER) {
	if (recv(sock, buff, size, 0) == ARF_SOCKET_ERROR) {
		HandleError(APIGetLastError(), errorHandler); isConnected = false; return;
	}
}

inline void arf::TCPBase::RecvLoop(ARF_RECV_HANDLER, ARF_DISCONNECT_HANDLER, ARF_ERROR_HANDLER) {
	std::unique_ptr<char> buff(new char[ARF_BUFFSIZE]);
	std::fill(buff.get(), buff.get() + ARF_BUFFSIZE, 0);

	while (true) {
		if (exitThreads) { exitThreads = true; return; }

		if (recv(sock, buff.get(), ARF_BUFFSIZE, 0) == ARF_SOCKET_ERROR) {
			ThreadHandleError(APIGetLastError(), disconnectHandler); isConnected = false; return;
			isConnected = false;
		}

		if (recvHandler != nullptr)
			if (!recvHandler(this, buff.get(), ARF_BUFFSIZE)) return;

		std::fill(buff.get(), buff.get() + ARF_BUFFSIZE, 0);
	}
}

inline void arf::TCPBase::StartRecvLoop(ARF_RECV_HANDLER, ARF_DISCONNECT_HANDLER, ARF_ERROR_HANDLER) {
	recvThread = std::thread(&arf::TCPBase::RecvLoop, this, recvHandler, disconnectHandler, nullptr);
	recvThread.detach();
}

inline void arf::TCPBase::StopRecvLoop() {
	exitThreads = true;
}

inline bool arf::TCPBase::IsConnected() {
	return isConnected;
}

inline void arf::TCPBase::Disconnect(ARF_ERROR_HANDLER) {
	StopRecvLoop();

	if (shutdown(sock, ARF_SHUT_BOTH) == ARF_SOCKET_ERROR) {
		HandleError(APIGetLastError(), errorHandler); return;
	}

	isConnected = false;

	Close(errorHandler);
}

inline void arf::TCPBase::Close(ARF_ERROR_HANDLER) {
	if (ARF_CLOSE(sock) == ARF_SOCKET_ERROR) {
		HandleError(APIGetLastError(), errorHandler); return;
	}

	isConnected = false;
	}

/*inline arf::TCPBase::~TCPBase() {
	Close(nullptr);
}*/

// ==== TCPServer ====

// Constructor, block until client connects
inline arf::TCPServer::TCPServer(SockAddr listenSockAddr, ARF_ERROR_HANDLER) {
	if ((listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == ARF_INVALID_SOCKET) {
		HandleError(APIGetLastError(), errorHandler); isConnected = false; return;
	}

	sockaddr_in temp = listenSockAddr.GetSockAddr();
	if (bind(listenSock, (sockaddr*)&temp, sizeof(sockaddr_in)) == ARF_INVALID_SOCKET) {
		HandleError(APIGetLastError(), errorHandler); isConnected = false; return;
	}

	if (listen(listenSock, SOMAXCONN) == ARF_INVALID_SOCKET) {
		HandleError(APIGetLastError(), errorHandler); isConnected = false; return;
	}

	sockaddr_in peeraddr = { };

#ifdef _WIN32
	int addrLen = sizeof(sockaddr_in);
#else
	unsigned int addrLen = sizeof(sockaddr_in);
#endif

	if ((sock = accept(listenSock, (sockaddr*)&peeraddr, &addrLen)) == ARF_INVALID_SOCKET) {
		HandleError(APIGetLastError(), errorHandler); isConnected = false; return;
	}

	isConnected = true;

	peerSockAddr = SockAddr(peeraddr);
}

inline arf::TCPServer::TCPServer(unsigned short listenPort, ARF_ERROR_HANDLER) {
	TCPServer(SockAddr(INADDR_ANY, listenPort), errorHandler);
}

// ==== TCPClient ====

// Constructor, block until connect or error
inline arf::TCPClient::TCPClient(SockAddr peerSockAddr, ARF_ERROR_HANDLER) {
	sock = ARF_INVALID_SOCKET;
	if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == ARF_INVALID_SOCKET) {
		HandleError(APIGetLastError(), errorHandler); isConnected = false; return;
	}

	sockaddr_in temp = peerSockAddr.GetSockAddr();
	if (connect(sock, (sockaddr*)&temp, sizeof(sockaddr)) == ARF_SOCKET_ERROR) {
		HandleError(APIGetLastError(), errorHandler); isConnected = false; return;
	}

	isConnected = true;
}

inline arf::TCPClient::TCPClient(SockAddr peerSockAddr, ARF_RECV_HANDLER, ARF_DISCONNECT_HANDLER, ARF_ERROR_HANDLER) {
	sock = ARF_INVALID_SOCKET;
	if ((sock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == ARF_INVALID_SOCKET) {
		HandleError(APIGetLastError(), errorHandler); return;
	}

	sockaddr_in temp = peerSockAddr.GetSockAddr();
	if (connect(sock, (sockaddr*)&temp, sizeof(sockaddr)) == ARF_SOCKET_ERROR) {
		HandleError(APIGetLastError(), errorHandler); return;
	}

	StartRecvLoop(recvHandler, disconnectHandler, errorHandler);

	isConnected = true;
}

// ==== TCPServerMulti ====

inline void arf::TCPServerMulti::HandleError(int err, ARF_ERROR_HANDLER) {
	//throw std::runtime_error("Unhandled error: " + std::to_string(err));
	if (errorHandler != nullptr)
		errorHandler(err);
}

inline void arf::TCPServerMulti::ThreadHandleError(SockAddr addr, int err, ARF_DISCONNECT_HANDLER) {
	if (disconnectHandler != nullptr)
		disconnectHandler(addr, ParseAPIError(err));
	else throw std::runtime_error("Disconnect handler can't be null, it also handles internal thread errors.");
}

//void AcceptLoop(ARF_ACCEPT_HANDLER, ARF_ERROR_HANDLER)
inline void arf::TCPServerMulti::AcceptLoop(ARF_ACCEPT_HANDLER, ARF_DISCONNECT_HANDLER) {
	while (true) {
		ARF_SOCKET clientSocket = ARF_INVALID_SOCKET;
		sockaddr_in clientAddr = { };
#ifdef _WIN32
		int addrLen = sizeof(sockaddr_in);
#else
		unsigned int addrLen = sizeof(sockaddr_in);
#endif

		if ((clientSocket = accept(listenSock, (sockaddr*)&clientAddr, &addrLen)) == ARF_INVALID_SOCKET) {
			ThreadHandleError(SockAddr(0, 0), APIGetLastError(), disconnectHandler); return;
		}

		u_long iMode = 1;

#ifdef _WIN32
		if (ioctlsocket(clientSocket, FIONBIO, &iMode) == ARF_SOCKET_ERROR) { // non-blocking
#else
		if (ioctl(clientSocket, FIONBIO, &iMode) == ARF_SOCKET_ERROR) {
#endif
			ThreadHandleError(SockAddr(0, 0), APIGetLastError(), disconnectHandler); return;;
		}

		SockAddr addr(clientAddr);
		TCPBase client(clientSocket, addr);

		if (acceptHandler != nullptr) {
			if (!acceptHandler(&client, addr)) return;
		}

		clients.push_back(AddrSock{ clientSocket, clientAddr });
		}
	}

inline void arf::TCPServerMulti::RecvLoop(ARF_RECV_HANDLER, ARF_DISCONNECT_HANDLER) {
	std::unique_ptr<char> buff(new char[ARF_BUFFSIZE]);
	//char *buff = new char[ARF_BUFFSIZE];
	std::fill(buff.get(), buff.get() + ARF_BUFFSIZE, 0);
	int err;

	while (true) {
		for (size_t i = 0; i < clients.size(); i++) {
			AddrSock c = clients[i];

			if (exitThreads) return;

			int r = recv(c.sock, buff.get(), ARF_BUFFSIZE, 0);
			if (r == ARF_SOCKET_ERROR) {
				if ((err = APIGetLastError()) != ARF_EWOULDBLOCK) {
					ThreadHandleError(SockAddr(c.addr), APIGetLastError(), disconnectHandler);
					//disconnectHandler(SockAddr(c.addr), );
					ARF_CLOSE(clients[i].sock);
					clients.erase(clients.begin() + i);
					continue;
				}
				}
			else if (r == 0) {
				disconnectHandler(SockAddr(c.addr), arf::Exception(Exception::NOERROROK, 0));
				ARF_CLOSE(clients[i].sock);
				clients.erase(clients.begin() + i);
				continue;
			}

			if (recvHandler != nullptr)
				if (!recvHandler(new TCPBase(c.sock, SockAddr(c.addr)), buff.get(), ARF_BUFFSIZE))
					return;

			std::fill(buff.get(), buff.get() + ARF_BUFFSIZE, 0);
			//std::this_thread::sleep_for(1ms);
			}
		}
	}

// Constructor
inline arf::TCPServerMulti::TCPServerMulti(SockAddr listenSockAddr, ARF_ACCEPT_HANDLER, ARF_RECV_HANDLER, ARF_DISCONNECT_HANDLER, ARF_ERROR_HANDLER) {
	if ((listenSock = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP)) == ARF_INVALID_SOCKET) {
		HandleError(APIGetLastError(), errorHandler); return;
	}

	sockaddr_in temp = listenSockAddr.GetSockAddr();
	if (bind(listenSock, (sockaddr*)&temp, sizeof(sockaddr_in)) == ARF_SOCKET_ERROR) {
		HandleError(APIGetLastError(), errorHandler); return;
	}

	if (listen(listenSock, SOMAXCONN) == ARF_SOCKET_ERROR) {
		HandleError(APIGetLastError(), errorHandler); return;
	}

	//std::function<void(arf::Exception)> threadErrorHandler = &arf::TCPServerMulti::ThreadErrorHandler;
	acceptThread = std::thread(&arf::TCPServerMulti::AcceptLoop, this, acceptHandler, disconnectHandler);
	recvThread = std::thread(&arf::TCPServerMulti::RecvLoop, this, recvHandler, disconnectHandler);
	acceptThread.detach();
	recvThread.detach();
}

inline void arf::TCPServerMulti::StopThreads() {
	exitThreads = true;
}

inline void arf::TCPServerMulti::SendBroadcast(char *buff, size_t size, ARF_ERROR_HANDLER) {
	for (AddrSock c : clients) {
		if (send(c.sock, buff, size, 0) == ARF_SOCKET_ERROR) {
			HandleError(APIGetLastError(), errorHandler); continue;
		}
	}
}

inline size_t arf::TCPServerMulti::GetClientCount() {
	return clients.size();
}

inline arf::TCPBase* arf::TCPServerMulti::GetClient(int idx) {
	return new arf::TCPBase(clients[idx].sock, clients[idx].addr);
}

inline arf::SockAddr arf::TCPServerMulti::GetClientAddr(int idx) {
	return arf::SockAddr(clients[idx].addr);
}

inline void arf::TCPServerMulti::Close(ARF_ERROR_HANDLER) {
	StopThreads();

	int err = 0;
	if (shutdown(listenSock, ARF_SHUT_BOTH) == ARF_SOCKET_ERROR)
		if ((err = APIGetLastError()) != ARF_EWOULDBLOCK)
			HandleError(err, errorHandler);

	if (ARF_CLOSE(listenSock) == ARF_SOCKET_ERROR)
		if ((err = APIGetLastError()) != ARF_EWOULDBLOCK)
			HandleError(err, errorHandler);

	for (AddrSock c : clients) {
		if (shutdown(c.sock, ARF_SHUT_BOTH) == ARF_SOCKET_ERROR)
			if ((err = APIGetLastError()) != ARF_EWOULDBLOCK)
				HandleError(err, errorHandler);

		if (ARF_CLOSE(c.sock) == ARF_SOCKET_ERROR)
			if ((err = APIGetLastError()) != ARF_EWOULDBLOCK)
				HandleError(err, errorHandler);
	}

	clients.clear();
}

/*inline arf::TCPServerMulti::~TCPServerMulti() {
	Close(nullptr);
}*/

#endif
