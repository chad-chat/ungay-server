#pragma once

#undef ERROR

#include "arfTCP.h"
#include "json.hpp"
#include <string>
using json = nlohmann::json;

using namespace std::chrono_literals;

enum threadID { MAIN_THREAD = 1, LOG_THREAD, SERVER_THREAD, HANDLER_THREAD };
enum logType { LOG_INFO, LOG_WARNING, LOG_ERROR, LOG_UNRECOVERABLE, LOG_SUCCESS };

struct UserCmd {
	arf::TCPBase* sock;
	json cmd;
};

struct Msg {
	std::string username;
	time_t timestamp;
	uint32_t msgid;
	std::string text;
};

extern json conf;

extern void log(int type, int from, std::string text);
extern void logLoop();
extern void waitStopLog();

extern bool startServer();

extern void command(UserCmd usercmd);
extern void handlerLoop();
extern void handlerStop();

extern bool initDatabase();
extern void createUser(std::string username, std::string passwd);
extern bool authenticateUser(std::string username, std::string passwd);
extern void pushMessage(std::string username, std::string message);
extern std::vector<Msg> getMessages(int bsize, int bn);